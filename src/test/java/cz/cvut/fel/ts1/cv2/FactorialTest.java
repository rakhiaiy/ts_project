package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FactorialTest {
    @Test
    public void factorialTest(){
        Factorial fact = new Factorial();
        long result = fact.factorial(5);
        assertEquals(120, result);
    }
}
