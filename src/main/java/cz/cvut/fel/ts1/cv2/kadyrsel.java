package cz.cvut.fel.ts1.cv2;

public class kadyrsel {
    public long factorial (int n) {
        if (n <= 1) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }
}
